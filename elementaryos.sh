echo "Actualizando todo"
apt-get update && apt-get upgrade -y

echo "Añadiendo repos"
add-apt-repository ppa:caffeine-developers/ppa
add-apt-repository ppa:versable/elementary-update

apt-get update

echo "Instalando esenciales"
apt-get install synapse caffeine elementary-thirdparty-icons libreoffice firefox vlc ubuntu-restricted-extras -y

echo "¿Quiere continuar [y/n]?"
read continuar
echo "Instalar flash"
apt-get install flashplugin-installer

echo "Instalar skype"
wget http://download.skype.com/linux/skype-ubuntu-precise_4.3.0.37-1_i386.deb -O skype.deb
dpkg -i skype.deb
apt-get -f install -y

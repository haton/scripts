#!/bin/bash
#to send a message use gs "hello #world"
#make a file .gsrc that contains the 3 following lines
# user="user"
# pass="pass"
# api="https://..."

source ~/.gsrc
curl -u $user:$pass $api -d status="$1"   
